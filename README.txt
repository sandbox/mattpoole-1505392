This module allows you to take portions of your page template and evaluate them with separators between each section
or a single section. The callback that powers this page returns no content, so it will basically output the wrapper
for the site. This module is useful if you need, for example, to copy the header and the footer for the site added on a
separate code base.

To access the wrapper visit /share_wrapper
To access a single section vist /share_wrapper/[SECTION-NAME]
To access the page as the anonymous user add /anonymous to the end of the above urls.

If you'd like to check if the wrapper page is being displayed so you can modify the output, check if the constant
SHARE_WRAPPER_ACTIVE is defined or if arg(0) is share_wrapper.