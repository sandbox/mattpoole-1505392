<?php
// This takes advantage that menu hooks automatically make a page-MENU_KEY templates_files[] entry
// in preprocess_page()
$share_wrapper = variable_get('share_wrapper', array());

$selected_section = strlen(arg(1)) && arg(1) != 'anonymous' ? arg(1) : "";
if ($selected_section) {
  if (isset($share_wrapper['section_values'][$selected_section])) {
    eval('?>' . $share_wrapper['section_values'][$selected_section]. '<?php;');
  }
  else {
    echo sprintf('Invalid section name: %s passed.<br />', check_plain($selected_section));
    if ($share_wrapper['sections']) {
      echo 'Valid names: ' . check_plain($share_wrapper['sections']) . '<br />';
    }
  }
}
else {
  $count = 1;
  // Loop thru sections instead of section values, because if they remove a section and save,
  // the value is still stored in the db untill the next save.
  foreach (_share_wrapper_sections_array_from_string($share_wrapper['sections']) as $section) {
    if (isset($share_wrapper['section_values'])) {
      if ($count > 1 && !$selected_section) {
        print _share_wrapper_delimiter();
      }
      $count++;
      eval('?>' . $share_wrapper['section_values'][$section]. '<?php;');
    }
  }
}

if (!strlen($share_wrapper['sections'])) {
  echo 'Please set up your sections first.<br />';
}

// If they wanted to be anonymous, reload whoever they were logged in to as before.
if (defined('SHARE_WRAPPER_ANONYMOUS_ACTIVE')) {
  global $user;
  $user = user_load(SHARE_WRAPPER_ANONYMOUS_ACTIVE);
}